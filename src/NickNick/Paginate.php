<?php 
    namespace NickNick;
    class Paginate extends PaginateHelpers {

        /**
         *    Holds the raw data that is parsed in.
         *    @var array
         */
        public $raw = [];

        /**
         *    Holds the total count of the raw data.
         *    @var integer
         */
        public $count = 0;
        
        /**
         *    Holds the number of items per page.
         *    @var integer
         */
        public $perPage = 15;

        /**
         *    Holds the the data for each page.
         *    @var null
         */
        public $items = null;

        /**
         *    Defines a custom get parameter for the pagination.
         *    We got to think about it all.
         *    @var string
         */
        public $queryName = 'page';

        /**
         *    Holds the parameters string for the links.
         *    @var string
         */
        public $parameters = '';

        /**
         *    Holds the link data generated from the data provided.
         *    @var array
         */
        public $links = [];

        /**
         *    Constructs the paginator.
         *    @param array     $data   
         *    @param integer   $perPage
         */
        public function __construct($data) {
        
            # Save the data as our raw data
            $this->raw = $data;

            # Save the count of the raw items.
            $this->count = count($this->raw);

            return $this;

        }

        /**
         *    Used to set a number if items per page.
         *    @param  int    $number
         *    @return void
         */
        public function items(int $number) {

            # Save the per page value.
            $this->perPage = $number;

            return $this;
        
        }

        /**
         *    Used to parse custom parameters into the paginator.
         *    @param  array  $input
         *    @return void
         */
        public function parameters(array $input) {

            $this->parameters = $this->generateParameters($input);

            return $this;

        }

        /**
         *    Used to set a custom parameter name for the page part.
         *    @param  string $str
         *    @return void
         */
        public function name($str) {
            if (!empty($str)) $this->queryName = $str;
            return $this;
        }

        /**
         *    Used to render the paginator.
         *    @return void
         */
        public function make() {

            # Generate the page items.
            $this->generatePageItems();

            # Generate link data.
            $this->generateLinks();
        
        }

    }