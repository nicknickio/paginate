<?php
    namespace NickNick;
    class PaginateHelpers {

        /**
         *    Used to generate the links for the pagination.
         *    @param  integer $page
         *    @return string
         */
        public function generateURL($page) {

            $name = $this->queryName;
            $parameters = $this->parameters;
            return "?{$name}={$page}" . $parameters;

        }

        /**
         *    Used to return the current page number!
         *    @return integer
         */
        public function getPageNumber() {

            if (isset($_GET[$this->queryName]) && (int)$_GET[$this->queryName]) {
                return $_GET[$this->queryName];
            } return 1;

        }

        /**
         *    Calculates the number of pages.
         *    @return integer
         */
        public function getPageCount() {

            # Calculate the number of pages.
            $pageCount = ceil($this->count / $this->perPage);

            # Return the calculated number of pages.
            return $pageCount == 0 ? 1 : $pageCount;

        }

        /**
         *    This method generates the page items.
         *    @return array
         */
        public function generatePageItems() {

            # Loop through the items and add the ones that 
            # is in the range for the current page.
            foreach($this->raw as $key => $item) {
                
                # Calculate the sexyness.
                $offset = ($this->getPageNumber() * $this->perPage) - $this->perPage;
                $limit  = ($this->getPageNumber() * $this->perPage) - 1;

                # Check if the item belongs to the current page.
                if ( ($key > $offset || $key == $offset) && ($limit > $key || $limit == $key) ) {
                    $this->items[] = $item;
                }
            
            }

        }

        /**
         *    Used to generate links for the pagination.
         *    @return void
         */
        public function generateLinks() {

            # Define some easy to use variables.
            $page = $this->getPageNumber();
            $count = $this->getPageCount();

            # Add the data for the 'previous'-button.
            $this->links['navigator']['previous'] = ($page == 1) ? [
                'page' => 1,
                'active' => false,
                'url' => $this->generateURL(1)
            ] : [
                'page' => $page - 1,
                'active' => true,
                'url' => $this->generateURL($page-1)
            ];

            # Add the page numbers.
            for ($i = 1; $i <= $count; $i++) {
                $this->links['pages'][] = [
                    'page' => $i,
                    'active' => ($this->getPageNumber() == $i) ? true : false,
                    'url' => $this->generateURL($i)
                ];
            }

            # Add the data for the 'next'-button.
            $this->links['navigator']['next'] = ($page == $count) ? [
                'page' => (int)$page,
                'active' => false,
                'url' => $this->generateURL((int)$page)
            ] : [
                'page' => ($page + 1),
                'active' => true,
                'url' => $this->generateURL($page+1)
            ];

        }

        /**
         *    Used to generate the url parameters from array to string.
         *    @param  array  $input
         *    @return string
         */
        public function generateParameters(array $input) {

            $output = '&';

            foreach($input as $key => $value) {
                $output .= "$key=$value&";
            } 

            return rtrim($output, '&');

        }

    }